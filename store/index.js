export const state = () => ({
    showSidebar: false
});
export const mutations = {
    toggle(state) {
        state.showSidebar = !state.showSidebar;
    }
};
export const getters  = {
    getSidebarState(state) {
        return state.showSidebar;
    }
};